#!/bin/sh

echo "This does NOT configure the sitebot, you have to do that yourself!"
echo "Did you remember to change $sitename?"
echo "UPDATE: maybe you want to use the other ctpn-ftpwho, if you're using glftpd 2.0a?"


# TODO: make a nice little loop here maybe?
tar xvfz File-ReadBackwards-1.02.tar.gz
cd File-ReadBackwards-1.02
perl Makefile.PL; make; make install
cd ..
rm -rf File-ReadBackwards-1.02
tar xvfz Time-Duration-1.02.tar.gz
cd Time-Duration-1.02
perl Makefile.PL; make; make install
cd ..
rm -rf Time-Duration-1.02
tar xvfz Time-modules-2003.1126.tar.gz
cd Time-modules-2003.1126
perl Makefile.PL; make; make install
cd ..
rm -rf Time-modules-2003.1126
gcc cptn-ftpwho.c -o /glftpd/bin/cptn-ftpwho
cp cptn-races.pl /glftpd/bin
chmod 755 /glftpd/bin/cptn-races.pl