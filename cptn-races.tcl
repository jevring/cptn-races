#########################################################################
# cptn-races.tcl                                                        #
# Copyright captain - 2003-11-27                                        #
# Find me as captain on EFnet and LinkNet                               #
# I might have stolen the layout from idlebotkick by                    #
# turranius here, and for that i'm orry, but it's just a simple script  #
#########################################################################


bind pub vo !races pub:cptn-races
## sitering use only
#bind pub vo !sitenameraces pub:cptn-races

proc pub:cptn-races {nick output binary chan text} { 
	set binary {/glftpd/bin/cptn-races.pl}
	set who [lindex $text 0]
	foreach line [split [exec $binary $who] "\n"] {
		putquick "PRIVMSG $chan :$line"
	}
}

putlog "cptn-races-1.* by captain loaded"