/*
 * This aims to create an ftpwho that has a defined grammar, or standard is probably more accurate, 
 * concerning what information should be displayed, where, and whatnot.
 * Unlike some other tools I've used, this aims to be parsable the same way ALL THE TIME, which isn't something
 * I can say for the other tools I've used.
 * People don't realize and appreciate the importance of correct code anymore, which is sad.
 * I Couldn't have done this without a good IPC tutorial: http://www.ecst.csuchico.edu/~beej/guide/ipc/
 *
 * I will try to do this differently from the original ftpwho.c, codewise, but there are only so many good
 * ways got get an array of data out of shared memory, so alot of it will look the same.
 * exit codes:
 * 2 - No users online (also prints "0" before it exits)
 * 3 - SHMAT (Shared Memory Attach) Failed
 * 4 - SHMDT (Shared Memory Detatch) Failed
 * 0 - no problems, execution as usual
 *
 *
 * FORMAT: "pid;username;action;filename;host;currentdir;groupid;speed(k/s);idletime;tagline"
 * 
 * TODO: Maybe add flags as to what the format should include
 * TODO: Show only uploaders, only downloades, only idlers etc., via commandline flags
 * TODO: if people could harass the glftpd people about some proper docs for the virtual memory, I'd be happy
 * TODO: pass commandline flags to presentInformation(),, if needed
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

/* Stealing the datatype from ftpwho.c, mine obviously has to look the same anyway */
struct USER {
  char   tagline[64];
  char   username[24];
  char   status[256];
  char   host[256];
  char   currentdir[256]; // it is uncertain if this will ALWAYS contain a filename if you're up or downloading but it seems to, altho sometimes it doesn't
  long   groupid;
  time_t login_time;
  struct timeval tstart;   
  unsigned long bytes_xfer; 
  pid_t  procid;
};

//default ipc-key for glftpd
static key_t key = 0x0000DEAD; 
static int numUsers = 0;
static struct USER *users;
//this needs to be here to contain the stats for the shm segment
static struct shmid_ds ipcbuf; 


int main(int argc, char *argv[]) {
	
	int rv;
	int shmid;
	/* Connect to the start of the datasegment (it'll already be created, therefore we don't need a size of permissions
	 * If it is NOT already created, that means that there are nore users online
	 */
    if ((shmid = shmget(key, 0,0)) == -1) {
        //exit with a value higher than 0, error, trigger on this if you want
		exit(2);
    }
	// do I really need the typecast for the -1 comparison?
	users = (struct USER *)shmat(shmid, 0, SHM_RDONLY);
	if (users == (struct USER *)(-1)) {
		perror("3-shmat Failed (could not access or read shared memory)");
		exit(3);
	}
	//get the length of what's in shared memory to  fnd out how many users we have by dividing the datablock with the size of the USER struct
	shmctl( shmid, IPC_STAT, &ipcbuf);
	numUsers = ipcbuf.shm_segsz / sizeof(struct USER);
	//pass commandline arguments to the function to determine what to present (TODO)
	rv = presentInformation();
	/* detach from the segment: */
    if (shmdt(users) == -1) {
        perror("4-shmdt failed (could not detach from shared memory segment)");
        exit(4);
    }
	return rv;
}
/*
 * Stole this very quickly from the original, will write my own soon, but I need this one for testing
 * and it's not pid you fuck-ass, it's uid, or possible oid, ad the original array was named online
 */
static double calcTime(int uid) {
	struct timeval tstop;
	double delta, rate;
	if (users[uid].bytes_xfer < 1) {
		return 0;
	}
	gettimeofday(&tstop, (struct timezone *)0 );
	delta = ((tstop.tv_sec*10.)+(tstop.tv_usec/100000.)) - ((users[uid].tstart.tv_sec*10.)+(users[uid].tstart.tv_usec/100000.));
	delta = delta/10.;
	rate = ((users[uid].bytes_xfer / 1024.0) / (delta));
	if (!rate) rate++;
	return (double)(rate);
}
/*
 * this goes over the data in the users array, and prints it according to the format
 * TODO: Accept arguments to determine what to present
 */
int presentInformation() {
	int i;
	int j;
	char filename[260];
	char action[10];
	for (i = 0; i < numUsers; i++) {
		int idletime = time(NULL) - users[i].tstart.tv_sec;
		// the only interesting users are the ones who have a real pid and who are logged in properly
		if (users[i].procid != 0 || !strcmp(users[i].username, "-NEW-")) {
			if (!strncasecmp(users[i].status, "APPE", 4) || !strncasecmp(users[i].status, "STOR", 4)) {
				/* Upload */
				//assign "upload" to the action type
				strcpy(action, "upload");
				//copy the filename without the APPE or STOR to the, well, "filename" string
				strcpy(filename, users[i].status+5);
			} else if (!strncasecmp(users[i].status, "RETR", 4)) {
				/* Download */
				//assign "download" to the action type
				strcpy(action, "download");
				//copy the filename without the RETR to the, well, "filename" string
				strcpy(filename, users[i].status+5);
			} else if(idletime > 5 && users[i].bytes_xfer == 0) {
				/* Idle */
				//assign "idle" to the action type
				strcpy(action, "idle");
				strcpy(filename, "0"); //there needs to be something here for ;(.+); patterns to match
			}
			//trim the last newlines (with a new CHAR, not a new STRING, hence the '' and not the "")
			j = strlen(filename);
			if (!isprint(filename[j-2])) {
				filename[j-2] = '\0';
			} else if (!isprint(filename[j-1])) {
				filename[j-1] = '\0';
			}
			//trim the filename from current dir
			printf("%d;%s;%s;%s;%s;%s;%d;%.1f;%d;%s\n",	users[i].procid, 
														users[i].username, 
														action, 
														filename, 
														users[i].host, 
														users[i].currentdir, 
														users[i].groupid, 
														calcTime(i), 
														idletime,
														users[i].tagline);
		}
	}
	return 0;
}
