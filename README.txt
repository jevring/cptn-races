
LEGAL:

Copyright captain - 2003-11-27
Find me as captain on EFnet and LinkNet
I hereby grant you full use of this application, under the condition that if you modify it or redistribute it, 
this Copyright notice still needs to be intact and included.
Also it would be nice if you told me about your changes, althought that is not required

REQUIRES: cptn-ftpwho or cptn-ftpwho-2.0a (both included)
REQUIRES: perl 5 (5.8 or 5.61 shouldn't matter)
REQUIRES: File::ReadBackwards 1.02 URL: http://search.cpan.org/CPAN/authors/id/U/UR/URI/File-ReadBackwards-1.02.tar.gz
REQUIRES: Time-modules-2003.1126 > Time::ParseDate URL: http://search.cpan.org/CPAN/authors/id/M/MU/MUIR/modules/Time-modules-2003.1126.tar.gz
REQUIRES: Time-Duration-1.02 URL: http://search.cpan.org/CPAN/authors/id/S/SB/SBURKE/Time-Duration-1.02.tar.gz


DISCLAIMER:
I take no responsibility what so ever for possible damages to anything or anything of that nature.
You use this application at you OWN RISK!

CONTAINS:
cptn-ftpwho.c
cptn-ftpwho-2.0a.c
cptn-races.pl
cptn-races.tcl
install.sh (not for 2.0, will not work unless you unpack the file directly on the unix system, i.e don't unpack in windows and then ftp the files over)
README.txt
File-ReadBackwards-1.02.tar.gz
Time-modules-2003.1126.tar.gz
Time-Duration-1.02.tar.gz


INSTALLATION:

- download and install cptn-ftpwho unless you already have it (included in the package)
  |-> NEW: package now includes cptn-ftpwho-2.0a.c, a working ftpwho for the new glftpd 2.0a. compile this instead of the regular ctpn-ftpwho.c if you use glftpd 2.0a
- compile ctpn-ftpwho and put it in your bindir, usually /glftpd/bin (gcc cptn-ftpwho.c -o /glftpd/bin/cptn-ftpwho)
- download and install the required perl modules from cpan.org (perl Makefile.PL; make; make install)(skip "make test")(read the respective README)(included)
- change the $sitename variable to something more suitable in cptn-races.pl
- put cptn-races.pl in /glftpd/bin/
- chmod 755 /glftpd/bin/cptn-races.pl
- add cptn-races.tcl to your bot config
- set possible general and ring-part triggers (only for those of you with siterings)
- if you're using glftpd.log mode, you need to tell your zipscript to echo what gets put into the log for the RACES tag

TROUBLESHOOTING:
- Make sure cptn-ftpwho returns normally
- Make sure you have the path to perl set correctly in cptn-races.pl
- Make sure all other pathes are correct
- Make sure the perl moduels are installed poperly
- Make sure you didn't chance the key for shared memory in glftpd (should be the standard 0x0000dead), if you have, edit cptn-ftpwho.c to reflect the changes

KNOWN ERRORS:
- On some systems that use UTF-8, you some times get errors about strange chars. I haven't been abe to reliably reproduce this error, therefore I haven't been able to fix it.
  |- The simple solution is to pipe STDERR to /dev/null by suffixing the command with ">/dev/null 2>&1", that way, the erors are never shown, and because the script still returns proper raceinfo, it'll be fine.

VERSION HISTORY
2003-11-27 - 1.0 - first public release
2003-*-* - 1.* - there were changes, forgot to update, introducted ctpn-ftpwho, solved a few bugs
2003-11-16 - 1.51 - fixed some formatting, docs and error messages.
2004-01-28 - 1.53 - added support for glftpd 2.0a
