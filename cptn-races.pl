#!/usr/bin/perl
###################################
#
# Copyright captain - 2003-11-27
# Find me as captain on EFnet and LinkNet
# I hereby grant you full use of this application, under the condition that if you modify it or redistribute it, 
# this Copyright notice still needs to be intact and included.
# Also it would be nice if you told me about your changes, althought that is not required
# REQUIRES: cptn-ftpwho
# REQUIRES: perl 5
# REQUIRES: File::ReadBackwards 1.02 URL: http://search.cpan.org/CPAN/authors/id/U/UR/URI/File-ReadBackwards-1.02.tar.gz
# REQUIRES: Time-modules-2003.1126 - Time::ParseDate URL: http://search.cpan.org/CPAN/authors/id/M/MU/MUIR/modules/Time-modules-2003.1126.tar.gz
# REQUIRES: Time-Duration-1.02 URL: http://search.cpan.org/CPAN/authors/id/S/SB/SBURKE/Time-Duration-1.02.tar.gz
#


# This script reads the output from tur-ftpinfo to see whos uploading, then it compares the files in the dir being raced
# against those in the sfv in the current dir to get a percentage.
# Does NOT support 0day, only races with sfv+mp3/rar/whatever

# TODO: verify glftpd.log writing (90% - TS)
# TODO: apply some kind of cookie system for display in glftpd.log (screw the cookie system, learn printf formatting instead)
# TODO: make sure everything that is excluded from the logs get put into the @excludedirs array, otherwise, duration will FAIL
# TODO: user $fullPathPrefix everywhere /glftpd is normally used, for ease of configuration
# TODO: support ALL script output via glftpd.log

use strict;
use diagnostics;


#for reading the logfile backwards
use File::ReadBackwards;
#easily convert the time from the log format into something we can use
#these two are now needed if you set $use_duration to 0, so you can just uncomment them, but they're really good to have otherwise
use Time::ParseDate;
use Time::Duration;

my @ftpWhoOutput;
my %races;
#used for error trapping
my $duration_ok = 0;
#These are the only interesting values, along with formatting at the bottom (which might be made into so kind of cookies later)
my $use_glftpd_log = 0;
my $use_duration = 1;
my $glftpd_log = "/glftpd/ftp-data/logs/glftpd.log";
#change this to "/jail/glftpd" if you're running jailed (or whatever patch you have to your glftpd dir)
my $fullPathPrefix = "/glftpd";
my @excludedirs = ("/site/groups/", "/site/private/", "sample");
my $sitename = "mysite";

#run a command, save the info in the filehandle (| at the END fo the command to read FROM it, otherwise, we write to it, if the pipe was in front)
open (FTPWHO, "/glftpd/bin/cptn-ftpwho|") || die "could not execute cptn-ftpwho, make sure it's installed and chmodded correctly";
#transfer the info from the filehandle into an array
@ftpWhoOutput = <FTPWHO>;
#close filehandle
close FTPWHO;

my $noRaces = 1;
#go through the array
foreach my $line (@ftpWhoOutput) {
	#parse the line and assign the info to variables
	$line =~ /(.+);(.+);(.+);(.+);(.+);(.+);(.+);(.+);(.+);(.+)/ig;
	my $pid = $1;
	my $user = $2;
	my $action = $3;
	my $speed = $8;
	my $release = $6;
	if ($action eq "upload") {
		# get the releasename, without path from the full releasepath
		# this will work, because perl tries to get as much as possible in the first quantifier
		# there's a problem, because sometimes the releasename doesn't contain the filename, for some odd reason, need more docs on this
		
		my $race;
		my $prefix;
		my $pre;
		# release contains a proper filename (as opposed to simply a dirname), check a certain pattern
		# crummy docs lead to conditionals like this
		if (-f $fullPathPrefix . $release) {
			$release =~ /(.+)\/(.+)\/(.+\....)$/;
			$race = $2;
			$prefix = $1;
			if ($race =~ /cd\d|dvd\d|disc\d$/i) {
				#found ourselves a subdir, create a new racename
				$prefix =~ /(.+)\/(.+)$/;
				$race = $2 . "/" . $race;
				$prefix = $1;
			}
		} else {
			#we should never end up here
			#print "we ended up somewhere we shouldn't have, need better documentation to glftpds shared memory, msg captain if you see this\n";
			$release =~ /(.+)\/(.+)$/;
			$race = $2;
			$prefix = $1;
			if ($race =~ /cd\d|dvd\d|disc\d$/i) {
				#found ourselves a subdir, create a new racename
				$prefix =~ /(.+)\/(.+)$/;
				$race = $2 . "/" . $race;
				$prefix = $1;
			}

		}
		#check to exclude predirs (should be 100% now)
		foreach my $excludedir (@excludedirs) {
			if ($release =~ /$excludedir/i) {
				$pre = 1;
			}
		}
		unless ($pre) {
			#flag if we've found atleast one release
			$noRaces = 0;
			#support a lising of the users for each race later?
			$races{$race}{'racers'}++;
			$races{$race}{'speed'} += $speed;
			$races{$race}{'fullPath'} = $fullPathPrefix . $prefix . "/" . $race;
			$races{$race}{'sitePath'} = $prefix . "/" . $race;
		}
	}
}
#exit if we haven't found any releases
if ($noRaces) {
	unless ($use_glftpd_log) {
		print "$sitename - No current races...\n";
	} else {
		#do glftpd.log related stuff here
		open (GLFTPDLOG, ">>$glftpd_log") || die "could not open glftpd.log\n";
		my $now = localtime;
		print GLFTPDLOG $now . " RACES: ";
		printf GLFTPDLOG "\"%s - No current races...\"\n",$sitename;	
		close GLFTPDLOG;
	}	
	exit 0;	
}

#check how many files are present
foreach my $race (keys %races) {
	my $file;
	my @sfv;
	my $path = $races{$race}{'fullPath'};
	#check that the dir actually exists (it might have gotten nuked in the progress)
	if (-d $path) {
		# open the dir to find the sfv and count the files
		opendir(RACE, $path) || die "could not opendir $path \n";
		#go over the files in the dir
		while(defined($file = readdir RACE)) {
			#make sure that they are indeed files
			if (-f $path . "/" . $file) {
				# we've found the sfv
				if ($file =~ /.+\.sfv/) {
					#open and cache the sfv
					open(SFV, $path . "/" . $file) || die "could not open SFV: " . $path . "/" . $file . "for reading\n";
					@sfv = <SFV>;
					close SFV;

					#EUREKA, loop over the sfv, and do a file (-f $f) check on all the VALID lines in it!
					foreach my $line (@sfv) {
						#if it's not a comment or a blank line
						unless ($line =~ /^[\s|;]/) {
							my ($filename) = ($line =~ /(.+) \w+/);
							if (-f $path . "/" . $filename) {
								$races{$race}{'completed'}++;
							}
							$races{$race}{'total'}++;
						}
					}
				}
			}
		}
		closedir RACE;

		if ($use_duration) {
			#check race start time in glftpd.log, reading backwards for faster response time
			#make sure to STOP READING once it's been found. (last)
			tie *LOG, 'File::ReadBackwards', $glftpd_log || die "can't open log backwards $!\n";
			my $sitepath = $races{$race}{'sitePath'};
			while (my $line = <LOG>) {
				if ($line =~ /(.+)NEWDIR...$sitepath/) {
					$races{$race}{'racestart'} = parsedate($1);
					$duration_ok = 1;
					#quit the loop since we found what we're looking for, saving ALOT of time
					last;
				}
			}
			close LOG;
		}
		unless ($duration_ok) {
			print $race . " could not be found in glftpd.log, make sure you have configured the exclude dirs correctly\n";
			exit 1;
		}
	}
}
#we've completed gathering the information. Now, depending on a flag at the top, we eiter write to glftpd.log or STDOUT

#loop over the races
foreach my $race (keys %races) {
	unless ($use_glftpd_log) {
		#print race info
		my $complete = ($races{$race}{'completed'} / $races{$race}{'total'});
		#if you get erros on the above line, it coupld be because of a faulty sfv file, 
		#since it can't count the total amoumt of files that are supposed to be in the release
		if ($use_duration) {
			my $duration = duration(time - $races{$race}{'racestart'});
			my $etaMultiplier = (1 - $complete) / $complete;
			#lasted 10s@75% -> 33% of the spent time is left, i.e 3s.
			#lasted 10s@25% -> 300% of the spent time is left, i.e. 30s
			my $eta = from_now($etaMultiplier * (time - $races{$race}{'racestart'}));
			printf "%s - %s - (%2d/%2d) files - %.1f%% done - %.1f kbps - %2d racer(s) - lasted: %s - ETA: %s\n",$sitename, $race ,$races{$race}{'completed'}, $races{$race}{'total'}, ($complete * 100), $races{$race}{'speed'}, $races{$race}{'racers'}, concise($duration), concise($eta);	
		} else {
			printf "%s - %s - (%2d/%2d) files - %.1f%% done - %.1f kbps - %2d racer(s)\n",$sitename, $race ,$races{$race}{'completed'}, $races{$race}{'total'}, ($complete * 100), $races{$race}{'speed'}, $races{$race}{'racers'};	
		}
	} else {
		#do glftpd.log related stuff here
		my $complete = ($races{$race}{'completed'} / $races{$race}{'total'});
		open (GLFTPDLOG, ">>$glftpd_log") || die "could not open glftpd.log\n";
		my $now = localtime;
		print GLFTPDLOG $now . " RACES: ";
		if ($use_duration) {
			my $duration = duration(time - $races{$race}{'racestart'});
			my $etaMultiplier = (1 - $complete) / $complete;
			#lasted 10s@75% -> 33% of the spent time is left, i.e 3s.
			#lasted 10s@25% -> 300% of the spent time is left, i.e. 30s
			my $eta = from_now($etaMultiplier * (time - $races{$race}{'racestart'}));
			printf GLFTPDLOG "\"%s - %s - (%2d/%2d) files - %.1f%% done - %.1f kbps - %2d racer(s) - lasted: %s - ETA: %s\"\n",$sitename, $race ,$races{$race}{'completed'}, $races{$race}{'total'}, ($complete * 100), $races{$race}{'speed'}, $races{$race}{'racers'}, concise($duration), concise($eta);	
		} else {
			printf GLFTPDLOG "\"%s - %s - (%2d/%2d) files - %.1f%% done - %.1f kbps - %2d racer(s)\"\n",$sitename, $race ,$races{$race}{'completed'}, $races{$race}{'total'}, ($complete * 100), $races{$race}{'speed'}, $races{$race}{'racers'};	
		}
		close GLFTPDLOG;
	}	
}
